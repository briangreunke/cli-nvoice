SHELL := /bin/bash
DOCKER_IMAGE_NAME:=cli-nvoice

build-img: ## Build the Docker image from Dockerfile
	@docker build -t $(DOCKER_IMAGE_NAME) .

build-default: ## Build the invoice using default parameters
	@docker run -it --rm \
		-v $(PWD):/usr/src/app \
		$(DOCKER_IMAGE_NAME) \
		build.py

build-custom: ## Build the invoice using passed in parameters
	@docker run -it --rm \
		-v $(PWD):/usr/src/app \
		$(DOCKER_IMAGE_NAME) \
		build.py \
		--template=$(template) \
		--data=$(data) \
		--stylesheet=$(stylesheet) \
		--basedir=$(basedir) \
		--output=$(output)

clean:	## Remove artifacts to include Docker image and invoice.pdf
	-@docker image rm $(DOCKER_IMAGE_NAME)

.PHONY: help

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help

