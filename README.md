# cli-nvoice

A CLI based invoice generator, using Python3.

cli-nvoice uses: 

- an HTML template file
- CSS stylesheet(s) (*optional*)
- images (*optional*)
- a YAML data file containing the invoice data

Simply include jinja2 style fields in the HTML template `{{ your_field }}`, an attribute in the YAML file of the same name `your_field: your_value`

## Setup

### Requirements

- Python3

### Python packages

- pyyaml
- jinja2
- weasyprint

Running `make install` will install the required packages using `pip`

```
make install
```

## Building

Build with the default values (path to template, stylesheet, etc.)

```
make build-default
```

Build with custom parameters

```
make build-custom template=path/to/HTMLtemplatefile data=path/to/datafile stylesheet=path/to/stylesheet basedir=path/to/basedir output=path/to/save/pdf
```

- `basedir`: the path to the location of stylesheets and images (referenced by weasyprint)
